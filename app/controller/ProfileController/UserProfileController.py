from flask import Flask, request, jsonify
from app import app
from app.model.Models import User
from app.service.UserProfileService.CreateProfileService import UserProfileService


class UserProfileController:

    # To create
    @app.route('/add', methods=['POST'])
    def getUser():
        json_data = request.json
        print(json_data)
        return UserProfileService.createUserDetails(json_data)

    # To show all
    @app.route('/show', methods=["GET"])
    def showAll():
        return UserProfileService.showUsers()
    
    # To update
    @app.route('/update', methods= ["PUT"])
    def updateData():
        json_data = request.json
        return UserProfileService.updateUser(json_data['id'],json_data['name'],json_data['company'],json_data['designation'])

    # To delete
    @app.route('/delete/<int:id>',methods=['DELETE'])
    def deleteUser(id):
        return UserProfileService.deleteUser(id)
