from flask import Flask, request,jsonify
import json
from app.model.Models import User, db
from sqlalchemy import create_engine
from app import con

class UserProfileService :
	
    # Create method
    def createUserDetails(a):
        user_log = userModel(
            name = a["name"],
            company = a["company"],
            designation = a["designation"],
        ) 

        db.session.add(user_log)
        db.session.commit()
        return jsonify({"message": "Sucess"})

    # Read method
    def showUsers():
        eng = create_engine(con)
        connection = eng.connect()
        data = connection.execute("SELECT * FROM user")
        # print(data)
        return jsonify([dict(row) for row in data])
        
        # for user in data: 
        #     print(user.name)
        # json_string = json.dumps(data)
        # print(json_string)
        # return jsonify({"dkf":'suce'})

    # Update method
    def updateUser(id,name,company,designation):
        UserUpdate = User.query.filter_by(id=id).first()
        UserUpdate.name = name
        UserUpdate.company = company
        UserUpdate.designation = designation
        db.session.commit()
        return jsonify({'message':"updated successfully"})

    # Delete method
    def deleteUser(id):
        User.query.filter_by(id=id).delete()
        db.session.commit()
        return jsonify({'message':"Delete successfully"})

userModel = User